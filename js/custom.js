$('.custum-check input').change(function(){
    if($(this).is(":checked")) {
      $(this).parent().addClass('checked');
    } else {
      $(this).parent().removeClass('checked');
    }
});


$(document).ready(function() {
    if ($(window).width() > 991) {
    var $scrollable_div1 = $('.side-navbar, .odds-panel, .bet-slip');
    if ($scrollable_div1.data('scrollator') === undefined) {
        $scrollable_div1.scrollator();}
    }
});

$('.calc-bet, .overlay-fade').on('click', function() {
    $('.calc-custom').fadeToggle("");
    $('.overlay-fade').toggleClass("on");
});

$(document).ready(function(){
    $(".fav-star i").on({
        click: function(){
            $(this).css("color", "#ffc501");
        }  
    });
});

$(document).ready(function(){
  $(".drawer").click(function(){ 
    $(".side-navbar").toggleClass('expand-nav');
    $(".odds-panel").toggleClass('mid-expand');
  });
});


$(document).ready(function(){
    $(".drawer").click(function(){ 
        $('.side-nav.expand-nav').find('.drawer img').toggleClass('close-arrow')
        $('.side-nav').find('.drawer img').toggleClass('open-arrow')
    });    
});

$(function(){
    var carousel = $('.carousel-calender');
    var carouselChild = carousel.find('li');
    var clickCount = 0;
    var canClick = true;
    
    itemWidth = carousel.find('li:first').width(); //Including margin
    
    //Set Carousel width so it won't wrap
    // carousel.width(itemWidth*carouselChild.length);
    
    //Place the child elements to their original locations.
    refreshChildPosition();

    //Set the event handlers for buttons.
    $('.btnNext').click(function(e){        
        if($(".carousel-calender").find("li:eq(4)").text()!=14) {
            if(canClick) {
                canClick = false;
                clickCount++;
                //Animate the slider to left as item width 
                carousel.stop(false, true).animate({
                    left : '-='+itemWidth
                },300, function(){
                    //Find the first item and append it as the last item.
                    lastItem = carousel.find('li:first');
                    lastItem.remove().appendTo(carousel);
                    lastItem.css('left', ((carouselChild.length-1)*(itemWidth))+(clickCount*itemWidth));
                    canClick = true;
                });
            }
        }
    });
    
    $('.btnPrevious').click(function(){
        if($(".carousel-calender").find("li:eq(0)").text()!=1) {
            if(canClick){
                canClick = false;
                clickCount--;
                //Find the first item and append it as the last item.
                lastItem = carousel.find('li:last');
                lastItem.remove().prependTo(carousel);
                
                lastItem.css('left', itemWidth*clickCount);          
                //Animate the slider to right as item width 
                carousel.finish(true).animate({
                    left: '+='+itemWidth
                },300, function(){
                    canClick = true;
                });
            }
        }
    });
    
    function refreshChildPosition(){
        carouselChild.each(function(){
            $(this).css('left', itemWidth*carouselChild.index($(this)));
        });
    }
});  

window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}

$(document).ready(function() {
   var owl = $('.owl-carousel');
    owl.owlCarousel({
        margin: 10,
        nav: false,
        autoplay : 200,
        loop: true,
        responsiveClass:true,
        dots: true,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 1
          },
          1200: {
            items: 1
          },
          1360: {
            items: 1
          }
        }
    })
})

$(document).ready(function() {
  var owl = $('.owl-carousel-1');
  owl.owlCarousel({
    nav: true,
    autoplay : false,
    loop: false,
    responsiveClass:false,
    dots: false,
    items: 4,
    navText:["<div class='nav-btn prev-slide'></div>","<div class='nav-btn next-slide'></div>"],
  })
})